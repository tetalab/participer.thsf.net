\echo ******************************
\echo * Dropping database participer_thsf
\echo ******************************

\c postgres;
drop database participer_thsf;

\echo **************************
\echo * Dropping role participer_thsf
\echo **************************
drop role participer_thsf;

\echo ***************************************************
\echo * Creating role participer_thsf with password participer_thsf
\echo ***************************************************
create role participer_thsf with LOGIN ENCRYPTED PASSWORD 'participer_thsf';

\echo ******************************
\echo * Creating database participer_thsf
\echo ******************************
create database participer_thsf;

\echo *******************************************
\echo * Giving participer_thsf ownership to participer_thsf
\echo *******************************************
alter database participer_thsf owner to participer_thsf;

\echo *********************************
\echo * Creating participer_thsf_users table
\echo *********************************

\c participer_thsf;
CREATE TABLE participer_thsf_users (
  id serial primary key,
  mail text not NULL,
  password text not NULL,
  name text,
  phone text,
  diet text,
  is_admin integer not NULL,
  link_id text
);


\echo *********************************
\echo * Creating participer_thsf_roles table
\echo *********************************

CREATE TABLE participer_thsf_roles (
  id serial primary key,
  role text not NULL,
  description text not NULL,
  num_slot integer not null default 2
);

\echo *********************************
\echo * Creating participer_thsf_turns table
\echo *********************************
CREATE TABLE participer_thsf_turns (
  id serial primary key,
  role_id integer not NULL,
  start_time timestamp not NULL,
  end_time timestamp not NULL,
  num_slot integer not NULL default 2,
  constraint fk_turns_role
     foreign key (role_id) 
     REFERENCES participer_thsf_roles (id)
);


\echo *********************************
\echo * Creating participer_thsf_staffs table
\echo *********************************
CREATE TABLE participer_thsf_staffs (
  id serial primary key,
  user_id integer not NULL,
  turn_id integer not NULL,
  slot_num integer not NULL,
  constraint fk_turns_turn_id
     foreign key (turn_id) 
     REFERENCES participer_thsf_turns(id),
  constraint fk_turns_staff_user_id
     foreign key (user_id) 
     REFERENCES participer_thsf_users (id)
);

\echo *************************************************
\echo * Giving participer_thsf_users ownership to participer_thsf
\echo *************************************************
alter table participer_thsf_users owner to participer_thsf;

\echo *************************************************
\echo * Giving participer_thsf_roles ownership to participer_thsf
\echo *************************************************
alter table participer_thsf_roles owner to participer_thsf;

\echo *************************************************
\echo * Giving participer_thsf_turns ownership to participer_thsf
\echo *************************************************
alter table participer_thsf_turns owner to participer_thsf;

\echo *************************************************
\echo * Giving participer_thsf_staffs ownership to participer_thsf
\echo *************************************************
alter table participer_thsf_staffs owner to participer_thsf;

\echo *********************************************************************
\echo * Inserting user demo identified by password demo to participer_thsf_users
\echo *********************************************************************
insert into participer_thsf_users (mail, password, name, phone, diet, is_admin) values ('bofh@tetalab.org',
                                                                                        '$2b$12$wm7PQ9IE7TYGYk1XB11mPusq7HsjYLYxt5G4v5Wz.jZbh5iWDHP5q',
                                                                                        'BOFH',
                                                                                        '0000000000',
                                                                                        'Omnivore',
                                                                                        1);

\echo *********************************************************************
\echo * Inserting roles to participer_thsf_roles
\echo *********************************************************************
insert into participer_thsf_roles (role, description) values ('Référent staff', '');
insert into participer_thsf_roles (role, description) values ('Référent run', '');
insert into participer_thsf_roles (role, description) values ('Référent bar', '');
insert into participer_thsf_roles (role, description) values ('Préparation/Installation/signalétique', 'Fermeture des zones inaccessibles au public|Prépa buvette : allumage tireuses et frigos 4h avant ouverture \+ approvisionnement des bars en produits destinés à la vente|Préparation des différents postes / stands selon besoins : PLN, billetterie, T-Shirts...|Prépa toilettes public : aspiration/pompage, nettoyage à grandes eaux, remplissage du produit, changement poubelles, approvisionnement PQ|Nettoyage lieu, réapprovisionnement poubelles aux endroits stratégiques');
insert into participer_thsf_roles (role, description) values ('Volante', 'Veiller à la sécurité du lieu : rondes, surveillance accès, gestion de crises, messager, remplaçant, etc... (collaboration avec référent staff)|Ramassage verre, check PQ, ménage ponctuel|Sur le dernier créneau horaire, nettoyage de deux blocs WC|Au moins une personne connaissant bien Myrys sur chaque créneau horaire');
insert into participer_thsf_roles (role, description) values ('P.L.N', 'Accueil public, explication du concept de PLN, tampons (si besoin)|Informer et guider le public (prog, espaces, toilettes,...)|Comptage du public');
insert into participer_thsf_roles (role, description) values ('Buvette ''bulle''','Service au bar et encaissement (caisse ou tickets boisson selon besoins)|Nettoyage régulier : bar, tireuses, cendriers, verres abandonnés...');
insert into participer_thsf_roles (role, description) values ('Bar ''concert''','Service au bar et encaissement (caisse ou tickets boisson selon besoins)|Nettoyage régulier : bar, tireuses, cendriers, verres abandonnés...');
insert into participer_thsf_roles (role, description) values ('Billeterie','Vente de tickets boisson / 1 ticket = 2,50 €|Poinçonnage des cartes VIP. 1 poinçon = 1 ticket = 1 euros|Tickets valables sur les 4 jours mais NON REMBOURSABLES');
insert into participer_thsf_roles (role, description) values ('Catering', 'Vérification de l''accès au catering (badge)|Service à l''assiette si besoin, gestion des quantités, réapprovisionnement du buffet|Maintenir la propreté des espaces catering (cuisine, buffet, tables, salle d''AG, tables extérieures, jardins)|Informer / rappeler le principe d''autogestion pour la vaisselle');
insert into participer_thsf_roles (role, description) values ('Ménage', 'Nettoyage général à la fermeture du lieu : ramasser verres / déchets / cendriers, sortir poubelles, cleaner surfaces (bar, stand,...) \+ balai');


\echo *********************************************************************
\echo * Inserting turns to participer_thsf_turns
\echo *********************************************************************
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (1, '2018-05-10 20:00:00', '2018-05-11 02:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (1, '2018-05-10 12:00:00', '2018-05-10 20:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (1, '2018-05-11 12:00:00', '2018-05-11 21:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (1, '2018-05-12 12:00:00', '2018-05-12 21:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (1, '2018-05-11 21:00:00', '2018-05-12 04:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (1, '2018-05-12 21:00:00', '2018-05-13 05:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (1, '2018-05-13 12:00:00', '2018-05-13 18:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (1, '2018-05-13 18:00:00', '2018-05-13 23:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (2, '2018-05-10 12:00:00', '2018-05-10 21:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (2, '2018-05-11 12:00:00', '2018-05-11 21:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (2, '2018-05-12 12:00:00', '2018-05-12 21:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (2, '2018-05-13 12:00:00', '2018-05-13 21:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (3, '2018-05-10 12:00:00', '2018-05-10 21:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (3, '2018-05-11 12:00:00', '2018-05-11 21:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (3, '2018-05-12 12:00:00', '2018-05-12 21:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (3, '2018-05-13 12:00:00', '2018-05-13 21:00:00', 1);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (4, '2018-05-08 14:00:00', '2018-05-08 18:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (4, '2018-05-09 14:00:00', '2018-05-09 18:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (4, '2018-05-10 13:00:00', '2018-05-10 15:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-10 14:00:00', '2018-05-10 17:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-10 17:00:00', '2018-05-10 20:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-10 20:00:00', '2018-05-10 23:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-10 23:00:00', '2018-05-11 02:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-10 14:00:00', '2018-05-10 17:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-10 17:00:00', '2018-05-10 20:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-10 20:00:00', '2018-05-10 23:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-10 23:00:00', '2018-05-11 02:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (9, '2018-05-10 20:00:00', '2018-05-10 23:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (9, '2018-05-10 23:00:00', '2018-05-11 02:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (7, '2018-05-10 14:00:00', '2018-05-10 17:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (7, '2018-05-10 17:00:00', '2018-05-10 21:00:00', 3);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (10, '2018-05-10 12:00:00', '2018-05-10 14:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (10, '2018-05-10 19:30:00', '2018-05-10 22:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (4, '2018-05-11 13:00:00', '2018-05-11 14:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-11 14:00:00', '2018-05-11 17:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-11 17:00:00', '2018-05-11 20:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-11 20:00:00', '2018-05-11 23:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-11 23:00:00', '2018-05-12 01:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-12 01:30:00', '2018-05-12 03:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-11 14:00:00', '2018-05-11 17:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-11 17:00:00', '2018-05-11 20:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-11 20:00:00', '2018-05-11 23:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-11 23:00:00', '2018-05-12 01:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-12 01:30:00', '2018-05-12 03:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (9, '2018-05-11 20:00:00', '2018-05-11 23:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (9, '2018-05-11 23:00:00', '2018-05-12 01:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (9, '2018-05-12 01:30:00', '2018-05-12 03:15:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (7, '2018-05-11 14:00:00', '2018-05-11 17:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (7, '2018-05-11 17:00:00', '2018-05-11 20:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (7, '2018-05-11 20:00:00', '2018-05-11 22:00:00', 3);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (8, '2018-05-11 20:00:00', '2018-05-11 23:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (8, '2018-05-11 23:00:00', '2018-05-12 01:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (8, '2018-05-12 01:30:00', '2018-05-12 03:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (10, '2018-05-11 12:00:00', '2018-05-11 14:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (10, '2018-05-11 19:30:00', '2018-05-11 22:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (4, '2018-05-12 12:00:00', '2018-05-12 13:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-12 13:30:00', '2018-05-12 16:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-12 16:30:00', '2018-05-12 19:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-12 19:30:00', '2018-05-12 22:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-12 22:30:00', '2018-05-13 01:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-13 01:30:00', '2018-05-13 04:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-12 13:30:00', '2018-05-12 16:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-12 16:30:00', '2018-05-12 19:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-12 19:30:00', '2018-05-12 22:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-12 22:30:00', '2018-05-13 01:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-13 01:30:00', '2018-05-13 04:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (9, '2018-05-12 19:30:00', '2018-05-12 22:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (9, '2018-05-12 22:30:00', '2018-05-13 01:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (9, '2018-05-13 01:30:00', '2018-05-13 04:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (7, '2018-05-12 13:30:00', '2018-05-12 16:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (7, '2018-05-12 16:30:00', '2018-05-12 19:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (8, '2018-05-12 19:30:00', '2018-05-12 22:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (8, '2018-05-12 22:30:00', '2018-05-13 01:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (7, '2018-05-12 19:30:00', '2018-05-12 22:00:00', 3);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (8, '2018-05-13 01:30:00', '2018-05-13 04:30:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (10, '2018-05-12 12:00:00', '2018-05-12 14:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (10, '2018-05-12 19:30:00', '2018-05-12 22:30:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (4, '2018-05-13 13:00:00', '2018-05-13 14:00:00', 4);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-13 14:00:00', '2018-05-13 17:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-13 17:00:00', '2018-05-13 20:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (5, '2018-05-13 20:00:00', '2018-05-13 22:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-13 14:00:00', '2018-05-13 17:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-13 17:00:00', '2018-05-13 20:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (6, '2018-05-13 20:00:00', '2018-05-13 22:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (7, '2018-05-13 14:00:00', '2018-05-13 17:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (7, '2018-05-13 17:00:00', '2018-05-13 19:00:00', 3);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (8, '2018-05-13 19:00:00', '2018-05-13 22:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (10, '2018-05-13 12:00:00', '2018-05-13 14:00:00', 2);
INSERT INTO participer_thsf_turns (role_id, start_time, end_time, num_slot) VALUES (10, '2018-05-13 19:30:00', '2018-05-13 21:30:00', 2);
