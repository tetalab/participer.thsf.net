function register() {
  var mail = document.getElementById('reg_mail').value;
  var password = document.getElementById('reg_password').value;
  var confirm = document.getElementById('reg_confirm').value;
  var regEmail = new RegExp('^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$','i');
  if (password.length < 8){
    alert("Le mot de passe doit avoir une longueur d'au moins 8 caractères");
    return false;
  }
  if (password != confirm){
    alert("Confirmation mot de passe incohérente");
    return false;
  }
  if (! regEmail.test(mail)){
    alert("Adresse email invalide");
    return false;
  }
  return true;
}

function update_account() {
  var password = document.getElementById('password').value;
  var confirm = document.getElementById('confirm').value;
  if (password != confirm){
    alert("Confirmation mot de passe incohérente");
    return false;
  }
  if (password.length > 0 && password.length < 8){
    alert("Le mot de passe doit avoir une longueur d'au moins 8 caractères");
    return false;
  }
  return true;
}

function delete_account(id) {
  if (confirm("La suppression d'un compte est définitive.\n\nConfirmer ?")) {
    document.location='/account/delete/'+id;
  }
}

function delete_turn(id) {
  if (confirm("La suppression d'un tour de staff est définitive.\n\nConfirmer ?")) {
    document.location='/turn/delete/'+id;
  }
}

function save_turn() {
  var start = document.getElementById('start').value;
  var end = document.getElementById('end').value;
  var s_start = start.split(':');
  var s_end = end.split(':');
  var regTime = new RegExp('^[0-9]{2}:[0-9]{2}:[0-9]{2}$','i');
  if (! regTime.test(start) || s_start[0] > 23 || s_start[1] > 59 || s_start[2] > 59){
    alert("Heure de début invalide.\n\nVeuillez respecter le format HH:MM:SS");
    return false;
  }
  if (! regTime.test(end) || s_end[0] > 23 || s_end[1] > 59 || s_end[2] > 59){
    alert("Heure de fin invalide.\n\nVeuillez respecter le format HH:MM:SS");
    return false;
  }
}

function update_sheet(obj, turn, slot) {
  var url = '/staffsheet/update/'+turn+'/'+slot;
  get_html_from_ajax(obj, url)
  obj.onclick=function (){ return clear_sheet(obj, turn, slot);}
  return true;
}

function clear_sheet(obj, turn, slot) {
  if (confirm("Voulez-vous vraiment libérer ce créneau ? \n\n Confirmer ?")) {
    var url = '/staffsheet/clear/'+turn+'/'+slot;
    get_html_from_ajax(obj, url);
    obj.onclick=function (){ return update_sheet(obj, turn, slot);}
  }
  return false;
}
